import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/FileManager.dart';
import 'package:test_app/FileManagerProvider.dart';
import 'file_uploader_screen.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
  var initializationSettingsIOS = IOSInitializationSettings();
  var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  runApp(
      Provider<FileManagerProvider>(
        create: (val) => FileManagerProvider(),
        child: MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Тестовое задание',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage()
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  BuildContext get context => super.context;

  FileManagerProvider _provider;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _provider = Provider.of<FileManagerProvider>(context);
  }


  void _navToFileUploadScreen() async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FileUploaderScreen(),
      ),
    );
  }

  void _clearFiles() {
    _provider.clearAllFiles();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home page'),
        ),
        body: Container(
          child: ListTile(
            title: Text("Файлы"),
            trailing: Icon(Icons.arrow_forward_ios),
            subtitle: _buildFileSubtitle(),
            onTap: _navToFileUploadScreen,
          ),
        ),
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _buildClearButton("Сбросить",_clearFiles),
            _buildSaveButton("Сохранить", () { _showNotification();})
          ],
        )
    );
  }

  Widget _buildFileSubtitle() {
    //var provider = Provider.of<FileManagerNotifier>(context);
    return StreamBuilder<FileManager>(
      stream: _provider.files,
      builder: (context,snapshot) {
        String resText = "Файлы не найдены";
        if (snapshot.data != null) {
          if (!snapshot.data.isEmpty) {
            if (snapshot.data.leftLoading != 0) {
              resText = "Всего файлов: ${snapshot.data.length}, Осталось загрузить: ${snapshot.data.leftLoading}";
            } else {
              resText = "Всего файлов: ${snapshot.data.length}";
            }
          }
        }

        return Text(resText);
      }
    );
  }

  Widget _buildClearButton(String text,Function func) {
    return StreamBuilder<FileManager>(
      stream: _provider.files,
      builder: (context,snapshot) {
        Function resValue;
        if (snapshot.data != null) {
          if (!snapshot.data.isEmpty){
            resValue = func;
          }
        }

        return FlatButton(
          child: Text(text),
          onPressed: resValue,
        );
      }
    );
  }

  Widget _buildSaveButton(String text,Function func) {
    return StreamBuilder<FileManager>(
        stream: _provider.files,
        builder: (context,snapshot) {
          Function resValue;
          if (snapshot.data != null) {
            if (!snapshot.data.isEmpty && snapshot.data.leftLoading == 0){
              resValue = func;
            }
          }

          return FlatButton(
            child: Text(text),
            onPressed: resValue,
          );
        }
    );
  }

  Future<void> _showNotification() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, 'Сохранить', 'Сохранено ${_provider.length.toString()} файлов', platformChannelSpecifics,
        payload: 'item x');
  }
}