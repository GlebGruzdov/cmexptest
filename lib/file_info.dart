import 'dart:async';

class FileInfo {
  String name;
  int time = 0;
  int maxTime;
  bool isRun = false;
  bool isFinish = false;
  Timer _t;
  Function _finishCallBack;

  FileInfo(this.name,this.maxTime) {
    time = 0;
  }

  void start(Function iFinish) async {
    isRun=true;
    _finishCallBack = iFinish;
    //await Future.delayed(Duration(seconds:maxTime),() => {});
    _t= Timer(Duration(seconds: maxTime),() {
      isRun=false;
      isFinish=true;
      if (_finishCallBack != null) {
        _finishCallBack();
      }
    });
//    isRun=false;
//    isFinish=true;
//    iFinish();
  }

  void stop() {
    _finishCallBack = null;
    if (_t != null) {
      _t.cancel();
    }
  }
}