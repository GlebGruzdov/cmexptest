import 'dart:async';
import 'dart:math';

import 'package:test_app/file_info.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test_app/FileManager.dart';

class FileManagerProvider {
  FileManager _files;
  final _filesStream = BehaviorSubject<FileManager>.seeded(new FileManager(null));
  Stream get files => _filesStream.stream;
  Sink get _changeFiles => _filesStream.sink;

  FileManagerProvider() {
    _files = new FileManager(changeState);
  }

  void addFile() {
    var rnd = new Random();
    _files.add(new FileInfo( "Файл "+_files.length.toString(),rnd.nextInt(5)+1));
    _changeFiles.add(_files);
  }

  void removeFile(int index) {
    _files.removeFile(index);
    _changeFiles.add(_files);
  }

  void changeState() {
    _changeFiles.add(_files);
  }

  int get length => _files.length;

  bool isEmpty() {
    return _files.isEmpty;
  }

  void clearAllFiles() {
    _files.clean();
    _changeFiles.add(_files);
  }

  void dispose() {
    _filesStream.close();
    this.dispose();
  }
}