import 'file_info.dart';

class FileManager {
  int _maxLoading = 3;
  int _nowLoading = 0;
  int _leftLoading = 0;
  int _maxFiles = 30;
  int get leftLoading => _leftLoading;

  final List<FileInfo> _files = new List<FileInfo>();
  List<FileInfo> get files => _files;
  int get length {
    return _files.length;
  }

  bool get isEmpty  => _files.isEmpty;

  Function _finishCallBack;

  FileManager(this._finishCallBack);

  bool canAdd() {
    return _files.length < _maxFiles;
  }

  void add(FileInfo file) {
    if (!canAdd()) {
      return;
    }
    _files.add(file);
    startNext();
    _leftLoading++;
  }

  void startNext() {
    if (_nowLoading >= _maxLoading) {
      return;
    }

    for(var i in _files) {
      if (!i.isFinish && !i.isRun) {
        if (_nowLoading >= _maxLoading){
          return;
        }

        i.start(finish);
        _nowLoading++;
      }
    }
  }

  void removeFile(int index) {
    var fileInfo = _files.removeAt(index);
    fileInfo.stop();
    if (fileInfo.isRun) {
      _nowLoading--;
    }
    if (!fileInfo.isFinish) {
      _leftLoading--;
    }
    startNext();
  }

  void finish() {
    _nowLoading--;
    _leftLoading--;
    _finishCallBack();
    startNext();
  }

  void clean() {
    _files.clear();
    _nowLoading = 0;
    _leftLoading = 0;
  }
}