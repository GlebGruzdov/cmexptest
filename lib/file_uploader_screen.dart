import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app/FileManagerProvider.dart';
import 'FileManager.dart';

class FileUploaderScreen extends StatefulWidget {
  @override
  _FileUploaderScreenState createState() => _FileUploaderScreenState();
}

class _FileUploaderScreenState extends State<FileUploaderScreen> {

  FileManagerProvider _provider;

  void _addFile() {
    _provider.addFile();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _provider = Provider.of<FileManagerProvider>(context);
  }

  void _removeFileByIndex(int index) {
    _provider.removeFile(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Файлы"),
      ),
      body: Container(
        child: _buildFileList(),
      ),
      floatingActionButton: _buildFloatingActionButton(),
    );
  }

  Widget _buildFileList() {
    //var provider = Provider.of<FileManagerNotifier>(context);
    return StreamBuilder<FileManager>(
      stream: _provider.files,
      builder: (context,snapshot) {
        if (snapshot.data == null) {
          return Center(
              child: Text('Нет файлов')
          );
        }

        if (snapshot.data.isEmpty) {
          return Center(
              child: Text('Нет файлов')
          );
        }

        return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, i) {
              return ListTile(
                title: Text(snapshot.data.files[i].name),
                subtitle: _buildSubtitle(snapshot.data.files[i].isRun, snapshot.data.files[i].isFinish),
                trailing: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    _removeFileByIndex(i);
                  },
                ),
              );
            }
        );
      }
    );
  }

  Widget _buildSubtitle(bool isRun,bool isFinish) {
    if (isFinish) {
      return null;
    } else {
      if (isRun) {
        return Text("Загружается");
      } else {
        return Text("В ожидании");
      }
    }

  }

  Widget _buildFloatingActionButton() {
    return StreamBuilder<FileManager> (
      stream:_provider.files,
      builder: (context,snapshot) {
        var isVisible = false;
        if (snapshot.data != null) {
          if (snapshot.data.canAdd()) {
            isVisible = true;
          }
        }

        return Visibility(
            visible: isVisible,
            child: FloatingActionButton(
              onPressed: _addFile,
              tooltip: 'Add file',
              child: Icon(Icons.add),
            )
        );
      });
  }
}